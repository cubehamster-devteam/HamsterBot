const URC = requireUncached('../classes/UserReactionCollector.js');

module.exports = async (info, toSend, buttons) => {
    let { msg, PublicInfo, oneInstanceOnly } = info;
    if (!PublicInfo.URCs) PublicInfo.URCs = [];

    let { URCs } = PublicInfo;

    oneInstanceOnly = true; //DEBUG;
    let prevURC = URCs.find(urc => urc.user.id == msg.author.id)
    if (prevURC)
        prevURC.collector.stop();

    let myMsg = await msg.channel.send(toSend);

    for (let button of buttons)
        await myMsg.react(button.emoji);

    const filter = (reaction, user) => user.id === msg.author.id;

    const collector = myMsg.createReactionCollector(filter, { time: 150000 });
    collector.on('collect', r => {
        // console.log(`Collected ${r.emoji.name}`);

        let button = buttons.find(x => x.emoji == r.emoji.name);

        if (button) {
            // console.log(myMsg);
            button.action(myMsg);
        }
    });
    collector.on('end', collected => {
        // console.log(`Collected ${collected.size} items`);

        // myMsg.edit('*This timed message expired*');
        if (oneInstanceOnly) myMsg.delete();
    });


    URCs.push(new URC(msg.author, collector));
}

/* HELPER FUNCTIONS */

// The reason for this function is that I can read JS files dynamically, which means 
// that I can edit bot behaivior without relaunching him in node
// This here is only for debug purposes
function requireUncached(module) {
    delete require.cache[require.resolve(module)]
    return require(module)
}