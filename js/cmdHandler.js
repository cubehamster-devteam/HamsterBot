const fs = require('fs');
const rndResponse = requireUncached('./rndResponse');

module.exports = (info) => {
    let { words, msg } = info;
    
    const commands = JSON.parse(fs.readFileSync('json/commands.json', 'utf8'));
    let command = commands[words[0]];

    if (command) {
        command.action = fillString({str: command.action, msg, words});
        
        let act = command.action;
        if (act.startsWith('$')) {
            // I'm doing splitting if any commands in the future will support
            // Multi arguments inside commands.json
            // Will delete if it remains unused
            let actWords = act.substring(1).split(" ");
            switch(actWords[0]) {
                case "print":
                    msg.channel.send(actWords.slice(1).join(" "));
                    break;
                case "reply":
                    msg.reply(actWords.slice(1).join(" "));
                    break;
                case "execute":
                    let Module = requireUncached(`./${actWords[1]}`);
                    
                    Module(info, actWords.slice(2));
                    break;
                default:
                    msg.channel.send("I guess MrShurukan did something wrong here");
                    break;
            }
        }
    }
    else {
        msg.reply(rndResponse('noSuchCommand'));
    }
};

// This function fills {} in a string with required constants
// And it fills (arg#) with arg number #
function fillString(info) {
    let { str, msg, words } = info; 

    const constants = {
        "NICKNAME": msg.author.username
    }
    for (let key in constants) {
        let rexp = new RegExp(`\\{${key}\\}`, 'g');
        str = str.replace(rexp, constants[key]);
    }

    for (let i = 1; i <= 10; i++) {
        let rexp = new RegExp(`(\\(arg${i}\\))`, 'g');
        // We don't need to plus or minus that 'i', i.e. arg1 is exactly in index 1
        str = str.replace(rexp, words[i]);
    }

    let rexp = /(\(all_args)\)/g;
    str = str.replace(rexp, words.slice(1).join(" "));

    return str;
}

// The reason for this function is that I can read JS files dynamically, which means 
// that I can edit bot behaivior without relaunching him in node
// This here is only for debug purposes
function requireUncached(module){
    delete require.cache[require.resolve(module)]
    return require(module)
}