const optionsMessage = requireUncached('../js/optionsMessage.js');

module.exports = async (info, toSend) => {

    //I store pages in public info because they are the same for all users
    let { PublicInfo } = info;
    if (!PublicInfo.pages) PublicInfo.pages = [
        'Page 1.\nWe used to lie out on the sand and let the sun dry us and try to guess the names of the birds singing.',
        'Page 2.\nI don\'t like sand. It\'s coarse and rough and irritating and it gets everywhere. Not like here. Here everything is soft and smooth.',
        'Page 3.\nDid you ever hear the tragedy of Darth Plagueis "the wise"?',
        'Page 4.\nI thought not. It\'s not a story the Jedi would tell you. It\'s a Sith legend. Darth Plagueis was a Dark Lord of the Sith, so powerful and so wise he could use the Force to influence the midichlorians to create life... He had such a knowledge of the dark side that he could even keep the ones he cared about from dying.'
    ];

    toSend = "This message has many pages. Use arrows to navigate between them.";
    buttons = [
        {
            emoji:'⬅', action: (message) => {
                if (page > 0) page-=1;
                message.edit(PublicInfo.pages[page]);
            } 
        },
        {
            emoji:'➡', action: (message) => {
                if (page < PublicInfo.pages.length - 1) page+=1;
                message.edit(PublicInfo.pages[page]);
            }
        }
    ];
    
    optionsMessage(info, toSend, buttons);
}

page = 0;

/* HELPER FUNCTIONS */

// The reason for this function is that I can read JS files dynamically, which means 
// that I can edit bot behaivior without relaunching him in node
// This here is only for debug purposes
function requireUncached(module) {
    delete require.cache[require.resolve(module)]
    return require(module)
}