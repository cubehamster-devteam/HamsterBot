const Discord = require('discord.js');
const fs = require('fs');

module.exports = (info) => {
    let { msg, prefix, V } = info;

    let sendOptionsMessage = requireUncached('./optionsMessage');

    let embed = new Discord.RichEmbed().setTitle('Help');
    // Light Blue-ish color
    embed.setColor('#3574e8');
    embed.setFooter(`HamsterBot ${V}`);

    let commands = JSON.parse(fs.readFileSync('json/commands.json'));

    /* Check for role of Dev */
    // Find a role with a name "Developer"
    let devRole = msg.guild.roles.find("name", "Developer");
    if (!msg.member.roles.has(devRole.id)) commands = commands.filter(x => x.debug != true);

    for (let cmdName in commands) {
        let command = commands[cmdName];
        let description = command.description;
        if (command.debug)
            description += "\n*This is a debug command*";

        embed.addField(prefix + cmdName, description + '\n');
        // embed.addBlankField();
    }

    info.delete = true;

    sendOptionsMessage(embed, info);
}

// The reason for this function is that I can read JS files dynamically, which means 
// that I can edit bot behaivior without relaunching him in node
// This here is only for debug purposes
function requireUncached(module){
    delete require.cache[require.resolve(module)]
    return require(module)
}