const fs = require('fs');

module.exports = (key) => {
    const responses = JSON.parse(fs.readFileSync('json/responses.json', 'utf8'))
    let response = responses[key];

    if (response) {
        let index = randomInt(0, response.length - 1);
        return response[index];
    }
    else
        return " ***MrShurukan screwed up*** ";
}


/* HELPER FUNCTIONS */
function randomInt(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}