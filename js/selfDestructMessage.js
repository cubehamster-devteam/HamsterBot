module.exports = async (info, toSend) => {
    let { msg, reply, seconds, deleteOriginal } = info;

    if (typeof toSend == 'object') {
        // Convert string to int
        seconds = ~~toSend[0];
        toSend = toSend.splice(1).join(" ");
    }

    let newMessages = [];

    if (reply) newMessages.push(await msg.reply(toSend));
    else newMessages.push(await msg.reply(toSend));

    newMessages.push(await msg.channel.send(`These messages will self-destruct in ${seconds} seconds`));

    setTimeout(() => {
        for (let m of newMessages) m.delete();
        
        if (deleteOriginal) msg.delete();
    }, seconds * 1000);
    // I'm doing * 1000 since time needs to be in ms
}