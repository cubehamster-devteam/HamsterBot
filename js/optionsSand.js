const optionsMessage = requireUncached('../js/optionsMessage.js');

module.exports = async (info, toSend) => {
    toSend = "Hello, would you like a sandwich?";
    buttons = [
        {emoji:'❌', action: (message) => message.edit('You dissmissed the thing')},
        {emoji:'🍔', action: (message) => message.edit("Here's your sandbitch 🥪")}
        // {emoji:'🅱️', action: () => msg.channel.print("🅱️oi")}
    ];
    
    optionsMessage(info, toSend, buttons);
}

/* HELPER FUNCTIONS */

// The reason for this function is that I can read JS files dynamically, which means 
// that I can edit bot behaivior without relaunching him in node
// This here is only for debug purposes
function requireUncached(module) {
    delete require.cache[require.resolve(module)]
    return require(module)
}