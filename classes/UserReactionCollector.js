class UserReactionCollector {
    constructor(user, collector) {
        this.user = user;
        this.collector = collector;
    }
}

module.exports = UserReactionCollector;