const Discord = require('discord.js');
const client = new Discord.Client();

const fs = require('fs');

const V = 'alpha-1.1.0';
const prefix = '!';

// For different modules to store their public info
const PublicInfo = {};

client.on('ready', () => {
    console.log(`${client.user.tag} is ready! Version ${V}`);
});

client.on('message', msg => {
    // Ignore all other bots
    if (msg.author.bot) return;

    let text = msg.content;
    if (text.startsWith(prefix)) {
        let words = text.substring(prefix.length).split(' ');
        try {
            requireUncached('./js/cmdHandler')({ words, msg, prefix, V, PublicInfo, requireUncached });
        }
        catch(e) {
            console.error('Something went really wrong\n');            
            console.log(e);
        }
    }
});

client.login(fs.readFileSync('txt/token.txt', 'utf8').trim());

/* HELPER FUNCTIONS */

// The reason for this function is that I can read JS files dynamically, which means 
// that I can edit bot behaivior without relaunching him in node
// This here is only for debug purposes
function requireUncached(module) {
    delete require.cache[require.resolve(module)]
    return require(module)
}